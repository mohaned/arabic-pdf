#!/usr/bin/env python
# -*- coding: utf-8 -*-

import fpdf
import arabic_reshaper

from bidi.algorithm import get_display


text = "كورس عربي"
print text

reshaped_text = arabic_reshaper.reshape(u"كورس عربي")
bidi_text = get_display(reshaped_text)

print bidi_text.encode('UTF-8')

pdf = fpdf.FPDF(format='letter')
pdf.add_page()
# pdf.set_font("Arial", size=12)
pdf.add_font('DejaVu', '', 'DejaVuSansCondensed.ttf', uni=True)
pdf.set_font('DejaVu', '', 14)
pdf.cell(200, 10, txt=bidi_text.encode('UTF-8'), align="C")
# pdf.cell(200, 10, txt=text, align="C")

pdf.output("C:\\Users\\MHM\\Desktop\\pdfs\\tutorial.pdf")
